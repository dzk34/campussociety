//
//  CSCoreDataStack.m
//  CampusSociety
//
//  Created by Khaled Noui on 11/12/2015.
//  Copyright © 2015 digiwabi. All rights reserved.
//

#import "CSCoreDataStack.h"

@implementation CSCoreDataStack
{
    NSString *_storeType;
    NSString *_modelName;
}

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;
@synthesize fetchedResultsController = _fetchedResultsController;

+ (CSCoreDataStack *)sharedCoreDataStack
{
    static CSCoreDataStack *_sharedCoreDataStack = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        _sharedCoreDataStack = [[self alloc] initWithStoreType:NSSQLiteStoreType modelName:COREDATA_STORE_NAME];
        
    });
    
    return _sharedCoreDataStack;
}


- (id) initWithStoreType:(NSString *)storeType modelName:(NSString *)modelName
{
    if (self = [super init]) {
        _storeType = storeType;
        _modelName = modelName;
    }
    
    return self;
}

- (void)saveContext
{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

#pragma mark - Core Data stack

- (NSManagedObjectContext *)managedObjectContext
{
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] init];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext;
}

- (NSManagedObjectModel *)managedObjectModel
{
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:_modelName withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    NSString *databaseName = [NSString stringWithFormat:@"%@.sqlite", _modelName];
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:databaseName];
    
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    
    NSDictionary *options = @{NSMigratePersistentStoresAutomaticallyOption : @YES,
                              NSInferMappingModelAutomaticallyOption : @YES};
    
    if (![_persistentStoreCoordinator addPersistentStoreWithType:_storeType
                                                   configuration:nil
                                                             URL:storeURL
                                                         options:options
                                                           error:&error]) {
        
        //Clear store, then recreate clean data stack...
        
        NSLog(@"Incompatible update, clearing store");
        [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil];
        
        [_persistentStoreCoordinator addPersistentStoreWithType:_storeType
                                                  configuration:nil
                                                            URL:storeURL
                                                        options:options
                                                          error:&error];
    }
    
    return _persistentStoreCoordinator;
}

#pragma mark - Application's Documents directory

- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}


#pragma mark - Application Models Management

+ (id)findOrCreate:(NSNumber *)objectID forEntity:(NSString*)entityName inContext:(NSManagedObjectContext *)moc
{
    id object = [self find:objectID forEntity:entityName inContext:moc];
    
    if (!object) {
        object = [NSEntityDescription insertNewObjectForEntityForName:entityName inManagedObjectContext:moc];
    }
    
    return object;
}

+ (id)find:(NSNumber *)objectID forEntity:(NSString*)entityName inContext:(NSManagedObjectContext *)moc
{
    NSEntityDescription *entity = [NSEntityDescription entityForName:entityName inManagedObjectContext:moc];
    
    return [self objectWithObjectID:objectID entity:entity inManagedObjectContext:moc];
}


+ (id)objectWithObjectID:(NSNumber *)objectId entity:(NSEntityDescription *)entity inManagedObjectContext:(NSManagedObjectContext *)moc
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"id == %@", objectId];
    NSFetchRequest *fr = [[NSFetchRequest alloc] init];
    fr.entity = entity;
    fr.predicate = predicate;
    fr.fetchLimit = 1;
    
    NSError *error = nil;
    
    NSArray *results = [moc executeFetchRequest:fr error:&error];
    
    if (!results && error) {
        NSLog(@"Error fetching object with ID: %@\nError: %@", objectId, error);
    }
    
    return [results count] ? results[0] : nil;
}

+ (NSFetchedResultsController*)fetchSavedData:(NSManagedObjectContext *)moc forPage:(NSInteger)page
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Feed"];
    [fetchRequest setSortDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"created_date" ascending:YES]]];
    fetchRequest.fetchOffset = page;
    fetchRequest.fetchLimit = FEED_PAGE_LIMIT;

    NSFetchedResultsController *fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:moc sectionNameKeyPath:nil cacheName:nil];
    //    [fetchedResultsController setDelegate:self];
    
    NSError *error = nil;
    [fetchedResultsController performFetch:&error];
    
    if (error) {
        NSLog(@"Unable to perform fetch.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    }
    
    return fetchedResultsController;
}

@end
