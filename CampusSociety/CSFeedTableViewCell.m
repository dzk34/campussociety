//
//  CSFeedTableViewCell.m
//  CampusSociety
//
//  Created by Khaled Noui on 11/12/2015.
//  Copyright © 2015 digiwabi. All rights reserved.
//

#import "CSFeedTableViewCell.h"
#import "CSFeed.h"

@implementation CSFeedTableViewCell

+ (UINib *)nib
{
    return [UINib nibWithNibName:NSStringFromClass([self class]) bundle:[NSBundle mainBundle]];
}

+ (instancetype)viewFromNib
{
    UINib *nib = [self nib];
    NSArray *parts = [nib instantiateWithOwner:nil options:nil];
    return [parts firstObject];
}

- (void)configureCell:(CSFeedTableViewCell *)cell using:(NSFetchedResultsController*)fetchedResultsController atIndexPath:(NSIndexPath *)indexPath
{
    CSFeed *record = [fetchedResultsController objectAtIndexPath:indexPath];

    [self.author setText:record.author.full_name];
    [self.authorMajor setText:record.author.major];
    [self.authorUni setText:record.author.uni];
    
    [self.content setText:record.content];
    [self.likesCount setText:[record.no_likes stringValue]];
    [self.commentCount setText:[record.no_comments stringValue]];
    
//        NSString *dateString = [jsonData objectForKey:@"created_date"];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"dd/MM/yyyy HH:mm"];
//        NSDate *feedDate = [dateFormatter dateFromString:record.created_date];
    NSString *feedDate = [dateFormatter stringFromDate:record.created_date];
    [self.createDate setText:feedDate];

    cell.selectionStyle = UITableViewCellSelectionStyleNone;
}

@end
