//
//  CSPostsFeedViewController.h
//  CampusSociety
//
//  Created by Khaled Noui on 11/12/2015.
//  Copyright © 2015 digiwabi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CSPostsFeedViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, NSFetchedResultsControllerDelegate>
{
    BOOL isLoadingNewPage;
    NSInteger currentPage;
}

@property (nonatomic, strong) IBOutlet UITableView *mainFeedTableView;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;

-(void)setupTableView;
-(void)loadSavedDataFromDatabase:(NSManagedObjectContext*)moc forPage:(NSInteger)page;

@end
