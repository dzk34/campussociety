//
//  CSAuthor+CoreDataProperties.m
//  CampusSociety
//
//  Created by Khaled Noui on 11/12/2015.
//  Copyright © 2015 digiwabi. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "CSAuthor+CoreDataProperties.h"

@implementation CSAuthor (CoreDataProperties)

@dynamic country_display;
@dynamic full_name;
@dynamic image;
@dynamic image_medium;
@dynamic id;
@dynamic major;
@dynamic uni;
@dynamic hasCommented;
@dynamic hasPosted;

@end
