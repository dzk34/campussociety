//
//  CSAccount.h
//  CampusSociety
//
//  Created by Khaled Noui on 11/12/2015.
//  Copyright © 2015 digiwabi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface CSAccount : NSManagedObject

+ (void) loginWithEmailAddress:(NSString *)email password:(NSString *)password context:(NSManagedObjectContext *)moc completion:(void (^)(NSDictionary *results, NSError *error))completion;

@end

NS_ASSUME_NONNULL_END

#import "CSAccount+CoreDataProperties.h"
