//
//  CSFeed+CoreDataProperties.m
//  CampusSociety
//
//  Created by khaledus on 11/12/2015.
//  Copyright © 2015 digiwabi. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "CSFeed+CoreDataProperties.h"

@implementation CSFeed (CoreDataProperties)

@dynamic attached_link;
@dynamic channel;
@dynamic channel_name;
@dynamic content;
@dynamic content_markup;
@dynamic created_date;
@dynamic embedded_content;
@dynamic id;
@dynamic is_a_favourite;
@dynamic is_liked_by_me;
@dynamic job;
@dynamic no_comments;
@dynamic no_likes;
@dynamic author;
@dynamic comments;

@end
