//
//  CSComment+CoreDataProperties.h
//  CampusSociety
//
//  Created by Khaled Noui on 11/12/2015.
//  Copyright © 2015 digiwabi. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "CSComment.h"

NS_ASSUME_NONNULL_BEGIN

@interface CSComment (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *body;
@property (nullable, nonatomic, retain) NSString *body_markup;
@property (nullable, nonatomic, retain) NSDate *created;
@property (nullable, nonatomic, retain) NSNumber *id;
@property (nullable, nonatomic, retain) NSString *last_child;
@property (nullable, nonatomic, retain) NSNumber *likes;
@property (nullable, nonatomic, retain) NSString *parent;
@property (nullable, nonatomic, retain) NSString *tree_path;
@property (nullable, nonatomic, retain) CSAuthor *author;
@property (nullable, nonatomic, retain) CSFeed *post;

@end

NS_ASSUME_NONNULL_END
