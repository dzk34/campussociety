//
//  CSAuthor+CoreDataProperties.h
//  CampusSociety
//
//  Created by Khaled Noui on 11/12/2015.
//  Copyright © 2015 digiwabi. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "CSAuthor.h"

NS_ASSUME_NONNULL_BEGIN

@interface CSAuthor (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *country_display;
@property (nullable, nonatomic, retain) NSString *full_name;
@property (nullable, nonatomic, retain) NSString *image;
@property (nullable, nonatomic, retain) NSString *image_medium;
@property (nullable, nonatomic, retain) NSNumber *id;
@property (nullable, nonatomic, retain) NSString *major;
@property (nullable, nonatomic, retain) NSString *uni;
@property (nullable, nonatomic, retain) CSComment *hasCommented;
@property (nullable, nonatomic, retain) CSFeed *hasPosted;

@end

NS_ASSUME_NONNULL_END
