//
//  CSAuthor.h
//  CampusSociety
//
//  Created by Khaled Noui on 11/12/2015.
//  Copyright © 2015 digiwabi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class CSComment, CSFeed;

NS_ASSUME_NONNULL_BEGIN

@interface CSAuthor : NSManagedObject

+ (CSAuthor *)userFromJson:(NSDictionary *)jsonData inContext:(NSManagedObjectContext *)moc;

@end

NS_ASSUME_NONNULL_END

#import "CSAuthor+CoreDataProperties.h"
