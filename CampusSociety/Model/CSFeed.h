//
//  CSFeed.h
//  CampusSociety
//
//  Created by Khaled Noui on 11/12/2015.
//  Copyright © 2015 digiwabi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface CSFeed : NSManagedObject

+ (void)fetchFeeds:(NSManagedObjectContext *)moc completion:(void (^)(NSArray *feeds, NSError *error))completion;
+ (void)fetchFeeds:(NSManagedObjectContext *)moc forPage:(NSInteger)page completion:(void (^)(NSArray *feeds, NSError *error))completion;
+ (NSArray *)feedsFromResults:(NSDictionary *)results inContext:(NSManagedObjectContext *)moc;
+ (CSFeed *)feedFromJson:(NSDictionary *)jsonData inContext:(NSManagedObjectContext *)moc;

@end

NS_ASSUME_NONNULL_END

#import "CSFeed+CoreDataProperties.h"
