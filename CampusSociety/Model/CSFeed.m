//
//  CSFeed.m
//  CampusSociety
//
//  Created by Khaled Noui on 11/12/2015.
//  Copyright © 2015 digiwabi. All rights reserved.
//

#import "CSFeed.h"
#import "CSAuthor.h"

@implementation CSFeed

+ (void)fetchFeeds:(NSManagedObjectContext *)moc completion:(void (^)(NSArray *feeds, NSError *error))completion
{
    CSHTTPClient *httpClient = [CSHTTPClient sharedClient];
    
    [httpClient callPath:@"2/search/?sort_by=recent&page=1"
                  method:HTTPGETMethod
                  params:nil
                 success:^(id results) {
                     NSLog(@"fetchFeeds SUCCESS");

                     __block NSArray *feedsArray;
                     dispatch_async(dispatch_get_main_queue(), ^{
                         
                         feedsArray = [self feedsFromResults:results inContext:moc];
                         [[CSCoreDataStack sharedCoreDataStack].managedObjectContext save:nil];
                         
                         completion(feedsArray, nil);
                     });
                     
                 } error:^(NSURLSessionDataTask *task, NSError *error) {
                     NSLog(@"fetchFeeds ERROR : %@", error);

                     completion(nil, error);
                 }];
}

+ (void)fetchFeeds:(NSManagedObjectContext *)moc forPage:(NSInteger)page completion:(void (^)(NSArray *feeds, NSError *error))completion
{
    NSLog(@"fetchFeeds for page : %d", page);
    
    CSHTTPClient *httpClient = [CSHTTPClient sharedClient];
    
    [httpClient callPath:[NSString stringWithFormat:@"2/search/?sort_by=recent&page=%d", page]
                  method:HTTPGETMethod
                  params:nil
                 success:^(id results) {
                     NSLog(@"fetchFeeds SUCCESS");
                     
                     __block NSArray *feedsArray;
                     dispatch_async(dispatch_get_main_queue(), ^{
                         
                         feedsArray = [self feedsFromResults:results inContext:moc];
                         [[CSCoreDataStack sharedCoreDataStack].managedObjectContext save:nil];
                         
                         completion(feedsArray, nil);
                     });
                     
                 } error:^(NSURLSessionDataTask *task, NSError *error) {
                     NSLog(@"fetchFeeds ERROR : %@", error);
                     
                     completion(nil, error);
                 }];

}

+ (NSArray *)feedsFromResults:(NSDictionary *)results inContext:(NSManagedObjectContext *)moc
{
    NSLog(@"feedsFromResults (%d)", [results count]);
    
    NSMutableArray *formattedFeed = [NSMutableArray array];
    
    for (NSDictionary *hash in [results objectForKey:@"results"]) {
        CSFeed *feed = [CSFeed feedFromJson:hash inContext:moc];
        if (feed)
            [formattedFeed addObject:feed];
    }
    
    return [NSArray arrayWithArray:formattedFeed];
}

+ (CSFeed *)feedFromJson:(NSDictionary *)jsonData inContext:(NSManagedObjectContext *)moc
{
    NSLog(@"feedFromJson (%d) %@", [jsonData count], jsonData);
    NSNumber *feedId = [jsonData objectForKey:@"id"];
    
    CSFeed *feed = [CSCoreDataStack findOrCreate:feedId forEntity:@"Feed" inContext:moc];
    
    feed.id = feedId;
    feed.content = [jsonData objectForKey:@"content"];
    feed.no_comments = [jsonData objectForKey:@"no_comments"];
    feed.no_likes = [jsonData objectForKey:@"no_likes"];
    
    NSString *dateString = [jsonData objectForKey:@"created_date"];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSZ"];
    NSDate *feedDate = [dateFormatter dateFromString:dateString];
    
    feed.created_date = feedDate;//[jsonData objectForKey:@"created_date"];
    
    
    CSAuthor *owner = [CSAuthor userFromJson:[jsonData objectForKey:@"author"] inContext:moc];
    
    [feed setValue:owner forKey:@"author"];
    
    return feed;
}

@end
