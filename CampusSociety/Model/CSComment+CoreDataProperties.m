//
//  CSComment+CoreDataProperties.m
//  CampusSociety
//
//  Created by Khaled Noui on 11/12/2015.
//  Copyright © 2015 digiwabi. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "CSComment+CoreDataProperties.h"

@implementation CSComment (CoreDataProperties)

@dynamic body;
@dynamic body_markup;
@dynamic created;
@dynamic id;
@dynamic last_child;
@dynamic likes;
@dynamic parent;
@dynamic tree_path;
@dynamic author;
@dynamic post;

@end
