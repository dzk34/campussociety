//
//  CSAuthor.m
//  CampusSociety
//
//  Created by Khaled Noui on 11/12/2015.
//  Copyright © 2015 digiwabi. All rights reserved.
//

#import "CSAuthor.h"
#import "CSComment.h"
#import "CSFeed.h"

@implementation CSAuthor

+ (CSAuthor *)userFromJson:(NSDictionary *)jsonData inContext:(NSManagedObjectContext *)moc
{
    NSNumber *userId = [jsonData objectForKey:@"id"];
    
    CSAuthor *user = [CSCoreDataStack findOrCreate:userId forEntity:@"Author" inContext:moc];
    
    user.full_name = [jsonData objectForKey:@"full_name"];
    user.major = [jsonData objectForKey:@"major"];
    user.uni = [jsonData objectForKey:@"uni"];
    
    return user;
}
@end
