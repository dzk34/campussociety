//
//  CSAccount.m
//  CampusSociety
//
//  Created by Khaled Noui on 11/12/2015.
//  Copyright © 2015 digiwabi. All rights reserved.
//

#import "CSAccount.h"

@implementation CSAccount

+ (void) loginWithEmailAddress:(NSString *)email password:(NSString *)password context:(NSManagedObjectContext *)moc completion:(void (^)(NSDictionary *results, NSError *error))completion
{
    NSString *deviceName = [[UIDevice currentDevice] name];
    
    NSDictionary *params = @{ @"email" : email, @"password" : password, @"device" : deviceName, @"hash" : @"1234" };
    
    CSHTTPClient *client = [CSHTTPClient sharedClient];
    
    [client callPath:@"email-auth/"
              method:HTTPPOSTMethod
              params:params
             success:^(id results) {
                 completion(results, nil);
             } error:^(NSURLSessionDataTask *task, NSError *error) {
                 completion(nil, error);
             }];
}

@end
