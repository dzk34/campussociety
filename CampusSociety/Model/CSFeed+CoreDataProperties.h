//
//  CSFeed+CoreDataProperties.h
//  CampusSociety
//
//  Created by khaledus on 11/12/2015.
//  Copyright © 2015 digiwabi. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "CSFeed.h"
#import "CSAuthor.h"
#import "CSComment.h"

NS_ASSUME_NONNULL_BEGIN

@interface CSFeed (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *attached_link;
@property (nullable, nonatomic, retain) NSNumber *channel;
@property (nullable, nonatomic, retain) NSString *channel_name;
@property (nullable, nonatomic, retain) NSString *content;
@property (nullable, nonatomic, retain) NSString *content_markup;
@property (nullable, nonatomic, retain) NSDate *created_date;
@property (nullable, nonatomic, retain) NSString *embedded_content;
@property (nullable, nonatomic, retain) NSNumber *id;
@property (nullable, nonatomic, retain) NSNumber *is_a_favourite;
@property (nullable, nonatomic, retain) NSNumber *is_liked_by_me;
@property (nullable, nonatomic, retain) NSString *job;
@property (nullable, nonatomic, retain) NSNumber *no_comments;
@property (nullable, nonatomic, retain) NSNumber *no_likes;
@property (nullable, nonatomic, retain) CSAuthor *author;
@property (nullable, nonatomic, retain) CSComment *comments;

@end

NS_ASSUME_NONNULL_END
