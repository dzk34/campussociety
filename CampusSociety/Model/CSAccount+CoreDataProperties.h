//
//  CSAccount+CoreDataProperties.h
//  CampusSociety
//
//  Created by Khaled Noui on 11/12/2015.
//  Copyright © 2015 digiwabi. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "CSAccount.h"

NS_ASSUME_NONNULL_BEGIN

@interface CSAccount (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *email;

@end

NS_ASSUME_NONNULL_END
