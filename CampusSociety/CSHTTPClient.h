//
//  CSHTTPClient.h
//  CampusSociety
//
//  Created by Khaled Noui on 10/12/2015.
//  Copyright © 2015 digiwabi. All rights reserved.
//

#import "AFHTTPSessionManager.h"

@protocol CSHTTPClientDelegate;

#define HTTPGETMethod @"GET"
#define HTTPPOSTMethod @"POST"
#define HTTPPUTMethod @"PUT"
#define HTTPDELETEMethod @"DELETE"

@interface CSHTTPClient : AFHTTPSessionManager
{
    NSString *authenticationToken;
}

@property (nonatomic, weak) id<CSHTTPClientDelegate> delegate;

+ (CSHTTPClient *)sharedClient;

// method used by the different parts of the app to access the API
- (void) callPath:(NSString *)path
           method:(NSString *)method
           params:(NSDictionary *)params
          success:(void (^)(id object))successBlock
            error:(void (^)(NSURLSessionDataTask *task, NSError *error))errorBlock;

- (void) setAuthenticationToken:(NSString *)authToken;

@end
