//
//  CSCoreDataStack.h
//  CampusSociety
//
//  Created by Khaled Noui on 11/12/2015.
//  Copyright © 2015 digiwabi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CSCoreDataStack : NSObject

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;

+ (CSCoreDataStack *)sharedCoreDataStack;
+ (id)findOrCreate:(NSNumber *)objectID forEntity:(NSString*)entityName inContext:(NSManagedObjectContext *)moc;
+ (id)find:(NSNumber *)objectID forEntity:(NSString*)entityName inContext:(NSManagedObjectContext *)moc;
+ (id)objectWithObjectID:(NSNumber *)objectId entity:(NSEntityDescription *)entity inManagedObjectContext:(NSManagedObjectContext *)moc;
+ (NSFetchedResultsController*)fetchSavedData:(NSManagedObjectContext *)moc forPage:(NSInteger)page;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;
- (id) initWithStoreType:(NSString *)storeType modelName:(NSString *)modelName;

@end
