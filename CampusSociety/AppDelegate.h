//
//  AppDelegate.h
//  CampusSociety
//
//  Created by Khaled Noui on 10/12/2015.
//  Copyright © 2015 digiwabi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

