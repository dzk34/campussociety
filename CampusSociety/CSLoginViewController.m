//
//  CSLoginViewController.m
//  CampusSociety
//
//  Created by Khaled Noui on 10/12/2015.
//  Copyright © 2015 digiwabi. All rights reserved.
//

#import "CSLoginViewController.h"
#import "CSAccount.h"

@interface CSLoginViewController ()

@end

@implementation CSLoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.loginView.layer.cornerRadius = 5;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)loginButtonTapped:(id)sender
{
    if (![self.emailField.text length] || ![self.passwordField.text length]) {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", @"") message:NSLocalizedString(@"Please type in your email address and password", @"") delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", @"") otherButtonTitles:nil];
        [alert show];
        
        return;
    }
    
    [self.emailField resignFirstResponder];
    [self.passwordField resignFirstResponder];
    
    [SVProgressHUD showWithStatus:NSLocalizedString(@"Signing in...", @"") maskType:SVProgressHUDMaskTypeGradient];

    [CSAccount loginWithEmailAddress:self.emailField.text
                            password:self.passwordField.text
                             context:[CSCoreDataStack sharedCoreDataStack].managedObjectContext
                          completion:^(NSDictionary *results, NSError *error) {
                              
                              [SVProgressHUD dismiss];

                              if([results objectForKey:@"token"])
                              {
                                  NSString *authToken = [results objectForKey:@"token"];
                                  NSLog(@"authToken=%@", authToken);
                                  
                                  CSHTTPClient *client = [CSHTTPClient sharedClient];
                                  [client setAuthenticationToken:authToken];
//                                  [manager.requestSerializer setValue:AUTHORIZATION_TOKEN forHTTPHeaderField:@"Authorization"];

                                  [self performSegueWithIdentifier:@"mainScreenSegue" sender:self];
                              }
                              else
                              {
                                  UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Signing in...", @"") message:NSLocalizedString(@"Sign in error", @"") delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", @"") otherButtonTitles:nil];
                                  [alert show];
                              }
                          }];
}

@end
