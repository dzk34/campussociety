//
//  CSPostsFeedViewController.m
//  CampusSociety
//
//  Created by Khaled Noui on 11/12/2015.
//  Copyright © 2015 digiwabi. All rights reserved.
//

#import "CSPostsFeedViewController.h"
#import "CSFeedTableViewCell.h"
#import "CSFeed.h"

#define FEED_CELL @"feedCell"

@interface CSPostsFeedViewController ()

@end

@implementation CSPostsFeedViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    isLoadingNewPage = NO;
    
    [self setupTableView];
    NSManagedObjectContext *moc = [CSCoreDataStack sharedCoreDataStack].managedObjectContext;
    
    self.fetchedResultsController.delegate = self;
    
    
    [CSFeed fetchFeeds:moc forPage:1 completion:^(NSArray *feeds, NSError *error) {
        NSLog(@"feeds : %@", feeds);
        [self loadSavedDataFromDatabase:moc forPage:0];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [[self.fetchedResultsController sections] count];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSArray *sections = [self.fetchedResultsController sections];
    id<NSFetchedResultsSectionInfo> sectionInfo = [sections objectAtIndex:section];
    
    return [sectionInfo numberOfObjects];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CSFeedTableViewCell *cell = (CSFeedTableViewCell*)[tableView dequeueReusableCellWithIdentifier:FEED_CELL];
    
    if(cell == nil)
    {
        cell = [[CSFeedTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:FEED_CELL];
    }
    
    [cell configureCell:cell using:self.fetchedResultsController atIndexPath:indexPath];
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 300;
}


- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
//    self.fetchedResultsController = [CSCoreDataStack fetchSavedData:moc forPage:page];
//    
//    if ([self.fetchedResultsController.fetchedObjects count]) {
//    }

    
    
    if (indexPath.row == [self.fetchedResultsController.fetchedObjects count] - 1 && currentPage != -1)
    {
        NSManagedObjectContext *moc = [CSCoreDataStack sharedCoreDataStack].managedObjectContext;

        NSInteger lastSectionIndex = [self.mainFeedTableView numberOfSections] - 1;
        NSInteger lastRowIndex = [self.mainFeedTableView numberOfRowsInSection:lastSectionIndex] - 1;
        
//        if (indexPath.row == lastRowIndex)
//            [self fetchFeedsForPage:++currentPage];
        
        
        if (indexPath.row == lastRowIndex)
        {
            [CSFeed fetchFeeds:moc forPage:currentPage completion:^(NSArray *feeds, NSError *error) {
                [self loadSavedDataFromDatabase:moc forPage:currentPage-1];
            }];
        }

    }
}



- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    [self.mainFeedTableView beginUpdates];
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    [self.mainFeedTableView endUpdates];
}


- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath {
    switch (type) {
        case NSFetchedResultsChangeInsert: {
            [self.mainFeedTableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
        }
        case NSFetchedResultsChangeDelete: {
            [self.mainFeedTableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
        }
        case NSFetchedResultsChangeUpdate: {
            break;
        }
        case NSFetchedResultsChangeMove: {
            [self.mainFeedTableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [self.mainFeedTableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
        }
    }
}


-(void)setupTableView
{
    [self.navigationController.navigationBar setBarStyle:UIBarStyleBlack];
    [self.navigationController.navigationBar setBarTintColor:[UIColor whiteColor]];
    
    self.navigationItem.titleView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"logo-red"]];
    
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 231)];
    UIImageView *headerImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"CampusSociety-header"]];
    [headerView addSubview:headerImageView];
    
    [self.mainFeedTableView setTableHeaderView:headerView];
    
    self.mainFeedTableView.contentInset = UIEdgeInsetsMake(-headerView.frame.size.height, 0, 0, 0);
    
    [self.mainFeedTableView registerNib:[CSFeedTableViewCell nib] forCellReuseIdentifier:FEED_CELL];
    
    self.mainFeedTableView.hidden = YES;
    
    [SVProgressHUD showWithStatus:NSLocalizedString(@"Fetching the feed...", @"") maskType:SVProgressHUDMaskTypeGradient];
}

-(void)loadSavedDataFromDatabase:(NSManagedObjectContext*)moc forPage:(NSInteger)page
{
    self.fetchedResultsController = [CSCoreDataStack fetchSavedData:moc forPage:page];
    
    if ([self.fetchedResultsController.fetchedObjects count]) {
        self.mainFeedTableView.hidden = NO;
        [self.mainFeedTableView reloadData];
    }
    
    [SVProgressHUD dismiss];
}

@end
