//
//  CSHTTPClient.m
//  CampusSociety
//
//  Created by Khaled Noui on 10/12/2015.
//  Copyright © 2015 digiwabi. All rights reserved.
//

#import "CSHTTPClient.h"

@implementation CSHTTPClient

+ (CSHTTPClient *)sharedClient
{
    static CSHTTPClient *_sharedClient = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        NSURL *baseURL = [NSURL URLWithString:API_URL];
        
        NSLog(@"Connected to API endpoint: %@", [baseURL absoluteString]);
        _sharedClient = [[self alloc] initWithBaseURL:baseURL];
    });
    
    return _sharedClient;
}

#pragma mark - Network Requests

- (void) callPath:(NSString *)path
           method:(NSString *)method
           params:(NSDictionary *)params
          success:(void (^)(id object))successBlock
            error:(void (^)(NSURLSessionDataTask *task, NSError *error))errorBlock
{
    
    NSString *url = [NSString stringWithFormat:@"%@%@", [[self baseURL] absoluteString], path];
        NSLog(@"Requesting URL: %@\nParams: %@\nMethod: %@", url, params, method);
    
    
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:url]];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [manager.requestSerializer setValue:authenticationToken forHTTPHeaderField:@"Authorization"];
    
    if ([method isEqualToString:HTTPPOSTMethod]) {
        [manager POST:url parameters:params progress:^(NSProgress *downloadProgress)
        {
            NSLog(@"Download Progress : %@", downloadProgress);
        }
        success:^(NSURLSessionDataTask *task, id responseObject)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                successBlock(responseObject);
            });
            
        }
        failure:^(NSURLSessionDataTask *task, NSError *error)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                errorBlock(task, error);
            });
            
        }];
    }
    
    if ([method isEqualToString:HTTPGETMethod]) {
        [manager GET:url parameters:params progress:^(NSProgress *downloadProgress)
         {
             NSLog(@"Download Progress : %@", downloadProgress);
         }
              success:^(NSURLSessionDataTask *task, id responseObject)
         {
             dispatch_async(dispatch_get_main_queue(), ^{
                 successBlock(responseObject);
             });
             
         }
              failure:^(NSURLSessionDataTask *task, NSError *error)
         {
             dispatch_async(dispatch_get_main_queue(), ^{
                 errorBlock(task, error);
             });
             
         }];
    }

}


- (void) setAuthenticationToken:(NSString *)authToken
{
    if (![authToken isEqualToString:authenticationToken]) {
        
        if (authToken == nil) {
            authToken = @"";
        }
        
        authenticationToken = [NSString stringWithFormat:@"Token %@", authToken];
    }
}

@end
