//
//  CSLoginViewController.h
//  CampusSociety
//
//  Created by Khaled Noui on 10/12/2015.
//  Copyright © 2015 digiwabi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CSLoginViewController : UIViewController

@property(nonatomic, weak) IBOutlet UIView *loginView;
@property(nonatomic, weak) IBOutlet UITextField *emailField;
@property(nonatomic, weak) IBOutlet UITextField *passwordField;

-(IBAction)loginButtonTapped:(id)sender;

@end
