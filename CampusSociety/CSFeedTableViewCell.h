//
//  CSFeedTableViewCell.h
//  CampusSociety
//
//  Created by Khaled Noui on 11/12/2015.
//  Copyright © 2015 digiwabi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CSFeedTableViewCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UILabel *author;
@property (nonatomic, strong) IBOutlet UILabel *authorMajor;
@property (nonatomic, strong) IBOutlet UILabel *authorUni;
@property (nonatomic, strong) IBOutlet UILabel *likesCount;
@property (nonatomic, strong) IBOutlet UILabel *commentCount;
@property (nonatomic, strong) IBOutlet UILabel *createDate;

@property (nonatomic, strong) IBOutlet UITextView *content;

+ (UINib *)nib;
+ (instancetype)viewFromNib;
- (void)configureCell:(CSFeedTableViewCell *)cell using:(NSFetchedResultsController*)fetchedResultsController atIndexPath:(NSIndexPath *)indexPath;

@end
